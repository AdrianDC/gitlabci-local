#!/usr/bin/env python3

# Standard libraries
from typing import List

# Stage type
Stage = str

# Stages type
Stages = List[Stage]
