#!/usr/bin/env python3

# Standard libraries
from typing import List

# Tag type
Tag = str

# Tags type
Tags = List[Tag]
