#!/usr/bin/env python3

# Standard libraries
from typing import List

# ScriptLine type
ScriptLine = str

# Scripts type
Scripts = List[ScriptLine]
