#!/usr/bin/env python3

# Standard libraries
from typing import Dict, Optional, Union

# AllowFailure type
AllowFailure = bool

# Retry type
Retry = int

# Trigger type
Trigger = Optional[Union[Dict[str, str], str]]

# When type
When = str
